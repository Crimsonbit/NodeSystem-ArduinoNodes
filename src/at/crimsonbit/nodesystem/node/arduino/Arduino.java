package at.crimsonbit.nodesystem.node.arduino;

import at.crimsonbit.nodesystem.gui.node.GNode;
import at.crimsonbit.nodesystem.node.IGuiNodeType;
import javafx.scene.paint.Color;

public enum Arduino implements IGuiNodeType {

	DEVICE("Device Node"), PORT("Port Node"), CLOSER("Device-Closer Node"), LISTENER("Arduino-Listener Node"), PIN(
			"Pin-Modifier Node"), READ_PIN("Read-Pin Node"), PIN_GET("Get-Pin Node"), PIN_SET_MODE(
					"Pin-Mode Node"), PIN_SET_VALUE("Pin-Value Node"), I2CDevice(
							"I2C Device Node"), I2CTRANSMIT("I2C Send Node"), I2CRECEIVE("I2C Receive Node");
	private String name;
	private static final Color ARDUINO_COLOR = new Color(0, (double) 152 / 255d, (double) 157 / 255d, 1);

	Arduino(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public Class<? extends GNode> getCustomNodeClass() {
		return ArduinoNodeClass.class;
	}

	@Override
	public Color getColor() {
		return ARDUINO_COLOR;
	}

}
